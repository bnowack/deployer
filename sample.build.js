//const fs = require('fs');
//const ini = require('ini');
//const readYaml = require('read-yaml');
let builder = require('bn-deployer').builder;

builder
    .init({
        env: {
            APP_ENV: 'prod',
            COMPOSER_HOME: '/c/ProgramData/ComposerSetup/bin/composer'
        },
        path: './releases',
        //version: ini.parse(fs.readFileSync('./config/application.ini', 'utf-8')).app.version, // .ini
        //version: readYaml.sync('config/services.yaml').parameters['app.version'], // symfony
        //version: JSON.parse(fs.readFileSync('./config/app-config.json', 'utf-8')).appVersion, // phue
        releaseDir: this.config.path + '/' + this.config.version
    })
    .resetBuildDirectory()
    .copy('config', `/config`)
    .copy('media', `/media`)
    .copy('src', `/src`)
    .copy('vendor/bnowack', `/vendor/bnowack`)
    .copy('vendor/carhartl/jquery-cookie/jquery.cookie.js', `/vendor/carhartl/jquery-cookie/jquery.cookie.js`)
    .copy('vendor/composer', `/vendor/composer`)
    .copy('vendor/jquery/jquery/dist', `/vendor/jquery/jquery/dist`)
    .copy('vendor/jrburke/requirejs/require.js', `/vendor/jrburke/requirejs/require.js`)
    .copy('vendor/phpmailer/phpmailer', `/vendor/phpmailer/phpmailer`)
    .copy('composer.json', `/composer.json`)
    .copy('.htaccess', `/.htaccess`)
    .copy('favicon.ico', `/favicon.ico`)
    .copy('index.php', `/index.php`)
    .copy('robots.txt', `/robots.txt`)
    .copy('dev/test-server.php', `/test-server.php`)
    .composer('dump-autoload --optimize --no-dev --classmap-authoritative --no-interaction')
    .remove(`/vendor/+(bin)`)
    .remove(`/vendor/**/+(docs|examples|test)`)
    .remove(`/vendor/**/+(+(README|readme).md|+(changelog|CHANGELOG).md|.gitignore|.DS_Store)`)
;
