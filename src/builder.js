const fs = require('fs-extra');
const axios = require('axios');
const glob = require('glob');
const spawn = require('child_process').spawn;
const execSync = require('child_process').execSync;
const mkdirp = require('mkdirp');
const chmodr = require('chmodr');
const replaceInFile = require('replace-in-file');

const builder = {

    config: {},

    init(config) {
        this.config = config;
        this.config.buildDir = this.config.path + '/' + this.config.version;
        // restore PATH if custom `env` is used
        if (this.config.env && !this.config.env.PATH) {
            this.config.env.PATH = process.env.PATH;
        }
        console.log(`* initialised builder: version ${this.config.version}, building to ${this.config.buildDir}`);
        return this;
    },

    resetBuildDirectory() {
        return this.resetDirectory(this.config.buildDir);
    },

    resetDirectory(dir) {
        console.log(`* resetting directory ${dir}`);
        return this
            .createDirectory(dir)
            .emptyDirectory(dir)
            ;
    },

    emptyDirectory(dir, remove) {
        console.log(`* ${remove ? 'removing' : 'clearing'} directory ${dir}`);
        fs.emptyDirSync(dir);
        if (remove) {
            fs.removeSync(dir);
        }
        return this;
    },

    createDirectory(dir) {
        console.log(`* creating directory ${dir}`);
        mkdirp.sync(dir);
        return this;
    },

    copy(from, to) {
        console.log(`* copying ${from} to ${to}`);
        if (to.match(/^\//)) {
            to = this.config.buildDir + to;
        }

        if (from.match(/^http/)) {
            fs.ensureFile(to);
            this.fetch(from).then((response) => {
                fs.writeFileSync(to, response.data, 'utf8');
            });
        } else {
            fs.copySync(from, to);
        }
        return this;
    },

    async fetch(url) {
        try {
            return await axios.get(url);
        } catch (error) {
            console.error(error);
        }
    },

    remove(pattern) {
        console.log(`* removing ${pattern}`);
        if (pattern.match(/^\//)) {
            pattern = this.config.buildDir + pattern;
        }
        let entries = glob.sync(pattern, {dot: true});
        entries.forEach((path) => {
            console.log('** removing', path);
            if (!fs.pathExistsSync(path)) {
                return;
            }

            if (fs.lstatSync(path).isDirectory()) {
                builder.emptyDirectory(path, true);
            } else {
                fs.removeSync(path);
            }
        });
        return this;
    },

    replace(filePatterns, from, to) {
        console.log(`* replacing ${from} with ${to} in ${filePatterns.join(', ')}`);
        replaceInFile.sync({
            files: filePatterns,
            allowEmptyPaths: true,
            from: new RegExp(from, 'g'),
            to: to
        });
        return this;
    },

    chmod(path, mode) {
        console.log(`* chmod'ing ${path} to ${mode}`);
        chmodr(path, mode, (err) => {
            if (err) {
                throw err;
            }
        });
        return this;
    },

    exec(command, args, options) {
        console.log(`* starting child process ${command}`);
        return new Promise((resolve, reject) => {
            options.shell = true;
            options.env = builder.config.env;
            const child = spawn(command, args, options);

            child.stdout.on('data', (data) => {
                console.log('**', command + ':', data.toString().trim());
            });

            child.stderr.on('data', (data) => {
                console.log('**', command + ':', data.toString().trim());
            });

            child.on('exit', (code) => {
                return (code === 0)
                    ? resolve()
                    : reject();
            });
        });
    },

    execSync(command, args) {
        console.log('* running', command, args);
        execSync(command + ' ' + args, {
            cwd: this.config.buildDir,
            env: this.config.env,
        });
        return this;
    },

    composer(args) {
        return this.execSync('composer', args);
    },

    encore(args) {
        return this.execSync('encore', args);
    },

    yarn(args) {
        return this.execSync('yarn', args);
    }
};

module.exports.builder = builder;
