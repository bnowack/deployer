# Deployer

## Usage

* copy `sample.build.js` to your project's bin directory (e.g. `bin/build-prod.js`).
* adjust the build script.
* run the build script with `node`.
